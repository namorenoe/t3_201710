package model.logic;

import java.util.ArrayList;

import api.IManejadorExpresiones;
import model.data_structures.Queue;
import model.data_structures.Stack;

public class ManejadorExpresiones implements IManejadorExpresiones{
	
	Stack<String> stack;
	Stack<String> auxStack;
	
	public ManejadorExpresiones() {
		// TODO Auto-generated constructor stub
		stack = new Stack<String>();
		auxStack = new Stack<String>();
	}
	
	@Override
	public boolean expresionBienFormada(String expresion) {
		// TODO Auto-generated method stub
		boolean expresionBienFormada = true;
		int parentesisCuadradoDerecha = 0;
		int parentesisCircularDerecha = 0;
		
		String[] listaCaracteresExpresion = expresion.split("");
		
		for(int i= listaCaracteresExpresion.length - 1; i>=0; i--)
		{
			stack.push(listaCaracteresExpresion[i]);
			auxStack.push(listaCaracteresExpresion[i]);
		}
		
		String item = null;
		while((item = stack.pop()) != null)
		{
			if( item.equals("[") )
			{
				parentesisCuadradoDerecha++;
			}
			if(item.equals("]"))
			{
				if(parentesisCuadradoDerecha > 0)
				{
					parentesisCuadradoDerecha --;
				}
				else{
					expresionBienFormada = false;
					break;
				}
			}
			if(item.equals("("))
			{
				parentesisCircularDerecha++;
			}
			if(item.equals(")"))
			{
				if(parentesisCircularDerecha > 0)
				{
					parentesisCircularDerecha --;
				}
				else{
					expresionBienFormada = false;
					break;
				}
			}
		}
		
		if(parentesisCuadradoDerecha > 0 || parentesisCircularDerecha > 0 )
		{
			expresionBienFormada = false;
		}
		
		if(expresionBienFormada == true)
		{
			Stack<String> pilaOrdenada = ordenarPila(auxStack);
			imprimirPila(pilaOrdenada);
		}
		
		return expresionBienFormada;
	}

	public void imprimirPila(Stack<String> stack)
	{
		String item = null;
		while((item = stack.pop()) != null)
		{
			System.out.println(item);
		}
	}
	
	
	public boolean esOperador(String caracter)
	{
		boolean siOperador = false;
		
		if(caracter.equals("+") || caracter.equals("-") || caracter.equals("*") || caracter.equals("/"))
		{
			siOperador = true;
		}
		
		return siOperador;
	}
	
	public boolean esParentesis(String caracter)
	{
		boolean siParentesis = false;
		
		if(caracter.equals("[") || caracter.equals("]") || caracter.equals("(") || caracter.equals(")"))
		{
			siParentesis = true;
		}
		
		return siParentesis;
	}
	
	@Override
	public Stack<String> ordenarPila(Stack<String> pila) {
		// TODO Auto-generated method stub
		ArrayList<String> listaExpresion = new ArrayList<>();
		Queue<String> cola = new Queue<>();
		Stack<String> pilaOrdenada = new Stack<>();
		
		String item = null;
		
		while((item = pila.pop()) != null)
		{
			listaExpresion.add(item);
		}
		
		for(int i= 0; i < listaExpresion.size(); i++)
		{
			if(esOperador(listaExpresion.get(i)) == false && esParentesis(listaExpresion.get(i)) == false)
			{
				cola.enqueue(listaExpresion.get(i));
			}
			
		}
		
		for(int i= 0; i < listaExpresion.size(); i++)
		{
			if(esOperador(listaExpresion.get(i)))
			{
				cola.enqueue(listaExpresion.get(i));
			}
		}
		
		for(int i= 0; i < listaExpresion.size(); i++)
		{
			if(esParentesis(listaExpresion.get(i)))
			{
				cola.enqueue(listaExpresion.get(i));
			}
		}
		
		String itemCola = null;
		while((itemCola = cola.dequeue()) != null)
		{
			pilaOrdenada.push(itemCola);
		}
		
		return pilaOrdenada;
	}

}
