package model.data_structures;

import java.util.Iterator;


public class ListaDobleEncadenada<T> implements ILista<T> {

	private NodoDoble<T> primero;
	private NodoDoble<T> ultimo;
	private int posicionActual;


	public ListaDobleEncadenada() {
		// TODO Auto-generated constructor stub
		primero = null;
		ultimo = null;
	}
	

	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void agregarElementoFinal(T elem) {
		// TODO Auto-generated method stub
		NodoDoble<T> nuevo = new NodoDoble<T>(elem);
		
		if (primero == null)
		{
			primero = nuevo;
			posicionActual = 0;
		}
		
		else{
		
			NodoDoble<T> NodoActual = primero;
			while(NodoActual.getSiguiente() != null)
			{
				NodoActual = NodoActual.getSiguiente();	
			}
			
			NodoActual.setSiguiente(nuevo);
			nuevo.setAnterior(NodoActual);
			posicionActual ++;
			
		}
			
	}
	
	public void agregarElementoInicio(T elem)
	{
		NodoDoble<T> nuevo = new NodoDoble<T>(elem);
		
		if(primero == null)
		{
			primero = nuevo;
			ultimo = primero;
			posicionActual = 0;
		}
		else
		{
			nuevo.setSiguiente(primero);
			primero.setAnterior(nuevo);
			primero = nuevo;
		}
	}

	@Override
	public T darElemento(int pos) {
		// TODO Auto-generated method stub
		
		if (primero == null)
		{
			return null;
		}

		NodoDoble<T> NodoActual = primero;
		NodoDoble<T> NodoRespuesta = null;
		
		int contador = 0;
		while(NodoActual != null)
		{
			
			if(contador == pos)
			{
				NodoRespuesta = NodoActual;
				break;
			}
			NodoActual= NodoActual.getSiguiente();
			contador++;
		}
		
		posicionActual = pos;
		return NodoRespuesta.getItem();
	}

	@Override
	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		int numeroElementos = 0;
		NodoDoble<T> actual = primero; 
		if(actual == null)
		{
			return 0;
		}
		while(actual!= null)
		{
			actual = actual.getSiguiente();
			numeroElementos++;
		}
		return numeroElementos;
	}

	@Override
	public T darElementoPosicionActual() {
		// TODO Auto-generated method stub
		return this.darElemento(posicionActual);
	}

	@Override
	public boolean avanzarSiguientePosicion() {
		// TODO Auto-generated method stub
		boolean avanzar = true;
		NodoDoble<T> actual = primero;
		if(actual.getSiguiente() == null)
		{
			avanzar = false;
		}
		return avanzar;
		
	}

	@Override
	public boolean retrocederPosicionAnterior() {
		// TODO Auto-generated method stub
		boolean retroceder = false;
		NodoDoble<T> actual = primero;
		if(actual.getAnterior() != null)
		{
			retroceder = true;
		}
		return retroceder;
	}
	
	
	public T eliminarPrimerItem()
	{
		NodoDoble<T> aux = primero;
		
		if(primero == null)
		{
			return null;
		}
		if(darNumeroElementos() != 0)
		{
			if(primero.getSiguiente() == null)
			{
				primero = null;
				ultimo = null;
			}
			else
			{
				primero = primero.getSiguiente();
				primero.setAnterior(null);
			}
		}
		return aux.getItem();
	}
	
	public T eliminarUltimoElemento()
	{
		NodoDoble<T> aux = ultimo;
		if(ultimo == null)
		{
			return null;
		}
		if(darNumeroElementos() != 0)
		{
			if(primero.getSiguiente() == null)
			{
				primero = null;
				ultimo = null;
			}
			else
			{
				ultimo = ultimo.getAnterior();
				ultimo.setSiguiente(null);
			}
		}
		return aux.getItem();
	}

}
