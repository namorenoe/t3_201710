package model.data_structures;

public interface IQueue<T> {
	
	/**
	 * Agrega un item en la última posición de la cola
	 * @param item
	 */
	public void enqueue(T item);
	
	/**
	 * Elimina el elemento en la primera posición de la cola
	 */
	public T dequeue();
	
	/**
	 * Indica si la cola está vacía
	 */
	public boolean isEmpty();

	/**
	 * Numero de elementos en la cola
	 */
	public int size();
	
}
