package model.data_structures;

public class NodoSencillo<T> {

	private T item;
	private NodoSencillo<T> siguiente;
	
	public NodoSencillo(T pItem, NodoSencillo<T> pNodo){
		item = pItem;
		siguiente = pNodo;
	}
	
	public void setSiguiente(NodoSencillo<T> pSiguiente){
		siguiente = pSiguiente;
	}
	
	public T getItem(){
		return item;
	}
	
	public NodoSencillo<T> getSiguiente(){
		return siguiente;
	}
	
}
