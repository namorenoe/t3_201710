package model.data_structures;

public class NodoDoble<T> {
	
	private T item;
	private NodoDoble<T> siguiente;
	private NodoDoble<T> anterior;
	
	public NodoDoble (T pItem)
	{
		item = pItem;
		siguiente = null;
		anterior = null;
	}
	
	public void setSiguiente(NodoDoble<T> siguiente )
	{
		this.siguiente = siguiente;
	}
	
	public void setAnterior(NodoDoble<T> anterior )
	{
		this.anterior = anterior;
	}

	public NodoDoble<T> getSiguiente()
	{
		return siguiente;
	}
	
	public NodoDoble<T> getAnterior()
	{
		return anterior;
	}
	
	public void setItem( T item)
	{
		this.item = item; 
	}
	
	public T getItem()
	{
		return item;
	}


}
