package model.data_structures;

import java.util.Iterator;

public class ListaEncadenada<T> implements ILista<T> {

	private NodoSencillo<T> primerNodo;
	private NodoSencillo<T> ultimoNodo;
	private int size;
	private int poscicionActual;


	@Override
	public Iterator<T> iterator() {
		Iterator<T> it = new Iterator<T>(){
			int index = -1;
			NodoSencillo<T> currentNode = primerNodo;
			@Override
			public boolean hasNext(){
				if (primerNodo == null || currentNode.getItem() == null){
					return false;
				}else
					return true;
			}
			@Override
			public T next() {
				if (index == -1){
					currentNode = primerNodo;
					index++;
					return (T) primerNodo.getItem();
				}else if (this.hasNext()){
					currentNode = currentNode.getSiguiente();
					index++;
					return (T) currentNode.getItem();
				}else
					return null;
			}
			@Override
			public void remove() {
				System.out.println("Metodo remover no implementado");
				//Sin usar	
			}
		};
		return it;
	}

	@Override
	public void agregarElementoFinal(T elem) {
		if (primerNodo == null){
			primerNodo = new NodoSencillo<T>(elem, null);
			ultimoNodo = primerNodo;
			size = 1;
			poscicionActual = 0;
		}else {
			NodoSencillo<T> add = new NodoSencillo<T>(elem, null);
			ultimoNodo.setSiguiente(add);
			ultimoNodo= add;
			size++;
			poscicionActual++;
		}

	}

	@Override
	public T darElemento(int pos){
		int index = -1;
		Iterator<T> iter = this.iterator();
		T res = null;
		while (iter.hasNext()&& index < pos){
			res = iter.next();
			index++;
		}
		if (index < pos-1 || res == null){
			return null;
		}else{
			poscicionActual = pos;
			return res;
		}

	}


	@Override
	public int darNumeroElementos() {
		Iterator <T> iter = this.iterator();
		int res = 0;
		while (iter.hasNext()){
			iter.next();
			res++;
		}
		return res;
	}

	@Override
	public T darElementoPosicionActual() {
		return this.darElemento(poscicionActual);
	}

	@Override
	public boolean avanzarSiguientePosicion() {
		if (darElemento(++poscicionActual)==null){
			return false;
		}else {
			return true;
		}

	}

	@Override
	public boolean retrocederPosicionAnterior() {
		if (darElemento(--poscicionActual)==null){
			return false;
		}else{
			return true;
		}
	}
	

}
