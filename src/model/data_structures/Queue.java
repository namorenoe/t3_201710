package model.data_structures;

public class Queue<T> implements IQueue<T> {

	private ListaDobleEncadenada<T> lista;
	
	public Queue()
	{
		lista = new ListaDobleEncadenada<>();
	}
	
	public T getItemAtTop()
	{
		T item = lista.darElemento(0);
		return item;
	}
	
	@Override
	public void enqueue(T item) {
		// TODO Auto-generated method stub
		lista.agregarElementoInicio(item);
	}

	@Override
	public T dequeue() {
		// TODO Auto-generated method stub
		return lista.eliminarPrimerItem();
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return lista.darNumeroElementos() == 0;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return lista.darNumeroElementos();
	}
	
	

}
