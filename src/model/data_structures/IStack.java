package model.data_structures;

public interface IStack<T> {
	
	/**
	 * Agregar un item al tope de la pila
	 * @param item
	 */
	public void push(T item);
	
	/**
	 * Elimina el elemento en el tope de la pila.
	 */
	public T pop();
	
	/**
	 * Indica si la pila esta vacia
	 */
	public boolean isEmpty();
	
	/**
	 * Numero de elementos en la pila
	 */
	public int size();

}
