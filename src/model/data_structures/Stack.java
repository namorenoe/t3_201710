package model.data_structures;

public class Stack<T> implements IStack<T> {
	
	private ListaDobleEncadenada<T> lista;
	
	public Stack()
	{
		lista = new ListaDobleEncadenada<T>();
	}

	public T getItemAtTop()
	{
		T item = lista.darElemento(0);
		return item;
	}
	
	@Override
	public void push(T item) {
		// TODO Auto-generated method stub
		lista.agregarElementoInicio(item);	
	}

	@Override
	public T pop() {
		// TODO Auto-generated method stub
		return lista.eliminarPrimerItem();
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return lista.darNumeroElementos() == 0;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return lista.darNumeroElementos();
	}
	
	

}
