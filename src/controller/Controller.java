package controller;

import api.IManejadorExpresiones;
import model.data_structures.Stack;
import model.logic.ManejadorExpresiones;

public class Controller {
	
	private static IManejadorExpresiones manejadorExpresiones = new ManejadorExpresiones();

	public static boolean expresionBienFormada(String expresion)
	{
		return manejadorExpresiones.expresionBienFormada(expresion);
	}
	
	public static void ordenarPila(Stack<String> pila)
	{
		manejadorExpresiones.ordenarPila(pila);
	}
}
