package api;

import model.data_structures.Stack;

public interface IManejadorExpresiones {

	/**
	 * Comprueba si la expresión esta bien formada
	 * @param String de la expresion
	 * @return false si esta mal formada, true si esta bien formada
	 */
	public boolean expresionBienFormada(String expresion);
	
	/**
	 * Ordena la pila que recibe por parametro
	 * @param Una pila de Strings
	 * @return La Pila ordenada
	 */
	public Stack<String> ordenarPila(Stack<String> pila);
	
	/**
	 * Verifica si un caracter es un operador binario
	 * @param caracter
	 * @return true si es operador
	 */
	public boolean esOperador(String caracter);
	
	/**
	 * Verifica si un caracter es un parentesis
	 * @param caracter
	 * @return true si es parentesis
	 */
	public boolean esParentesis(String caracter);
}
