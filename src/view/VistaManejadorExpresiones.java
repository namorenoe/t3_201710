package view;

import java.util.Scanner;

import controller.Controller;

public class VistaManejadorExpresiones {

	
	private static void printMenu()
	{
		System.out.println("------------Nicolás Moreno - 201615907-------------");
		System.out.println("---------------------Taller 3----------------------");
		System.out.println("1. Verificar, Ordenar e Imprimir la Expresión");
		System.out.println("2. Exit");
		System.out.println("Ingrese el numero de la opción para iniciar la tarea.");
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc=new Scanner(System.in);
		boolean fin=false;
		
		while(!fin){
			printMenu();
			
			int option = sc.nextInt();
			
			switch(option){
				case 1:
					System.out.println("Ingrese la expresión");
					String expresion = sc.next();
					boolean expresionFormada = Controller.expresionBienFormada(expresion);
					if(expresionFormada == false)
					{
						System.out.println("la expresión no está formada correctamente \n"
								+ "Intente de nuevo con otra expresión");
					}
					else{
						System.out.println("la expresión está formada correctamente \n"
								+ "la pila ordenada que representa la expresión es:");
					}
					break;
				case 2:
					fin=true;
					break;
			}
		}
	}

}
