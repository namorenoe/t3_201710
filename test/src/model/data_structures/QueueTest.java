package model.data_structures;

import static org.junit.Assert.*;

import org.junit.Test;

public class QueueTest {

	Queue<String> colaNueva = new Queue<String>();
	private String cadena = "";
	
	//Escenario 1
	public void SetUp()
	{
		int size = 5;
		int pos = 0;
		while (pos < size)
		{
			pos++;
			colaNueva.enqueue(cadena);
		}
	}
	
	//Escenario 2
	public void SetUp2()
	{
		int size = 5;
		int pos = 0;
		while (pos < size)
		{
			pos++;
			colaNueva.enqueue(cadena);
		}
		colaNueva.enqueue("dequeue");
	}
	
	@Test //Test de enqueue
	public void enqueueTest()
	{
		SetUp();
		colaNueva.enqueue("enqueueTest");
		assertEquals("enqueueTest", colaNueva.getItemAtTop());
	}
	
	@Test //Test de dequeue
	public void dequeueTest()
	{
		SetUp2();
		assertEquals("dequeue", colaNueva.dequeue());
	}
	
	@Test //Test de cantidad de Elementos
	public void cantidadElementosTest()
	{
		SetUp();
		assertEquals(5, colaNueva.size());
	}
	
	@Test //Test de isEmpty
	public void isEmptyTest()
	{
		SetUp();
		assertEquals(false, colaNueva.isEmpty());
	}
	

}


