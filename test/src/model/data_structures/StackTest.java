package model.data_structures;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

public class StackTest {

	Stack <String> pilaNueva = new Stack<String>();
	private String cadena = "";
	
	//Escenario 1
	public void SetUp()
	{
		int size = 5;
		int pos = 0;
		while (pos < size)
		{
			pos++;
			pilaNueva.push(cadena);
		}
	}
	
	public void SetUp2()
	{
		int size = 5;
		int pos = 0;
		while (pos < size)
		{
			pos++;
			pilaNueva.push(cadena);
		}
		pilaNueva.push("toPop");
	}
	
	@Test //Test de push
	public void pushTest()
	{
		SetUp();
		pilaNueva.push("pushTest");
		assertEquals("pushTest", pilaNueva.getItemAtTop());
	}
	
	@Test //Test de pop
	public void popTest()
	{
		SetUp2();
		assertEquals("toPop", pilaNueva.pop());	
	}
	
	@Test //Test de size
	public void cantidadElementos()
	{
		SetUp();
		assertEquals(5, pilaNueva.size());
	}
	
	@Test //Test de isEmpty
	public void isEmptyTest()
	{
		SetUp();
		assertEquals(false, pilaNueva.isEmpty());
	}
	
	
	
}
